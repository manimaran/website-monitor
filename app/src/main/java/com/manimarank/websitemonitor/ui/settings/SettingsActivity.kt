package com.manimarank.websitemonitor.ui.settings

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.material.switchmaterial.SwitchMaterial
import com.manimarank.websitemonitor.R
import com.manimarank.websitemonitor.databinding.ActivitySettingsBinding
import com.manimarank.websitemonitor.utils.Constants.IS_DARK_MODE_ENABLED
import com.manimarank.websitemonitor.utils.Constants.MONITORING_INTERVAL
import com.manimarank.websitemonitor.utils.Constants.MONITORING_STATUS_CODES
import com.manimarank.websitemonitor.utils.Constants.NOTIFY_ONLY_SERVER_ISSUES
import com.manimarank.websitemonitor.utils.Interval.nameList
import com.manimarank.websitemonitor.utils.Interval.valueList
import com.manimarank.websitemonitor.utils.SharedPrefsManager
import com.manimarank.websitemonitor.utils.SharedPrefsManager.set
import com.manimarank.websitemonitor.utils.Utils
import com.manimarank.websitemonitor.utils.Utils.getMonitorTime
import com.manimarank.websitemonitor.utils.Utils.isCustomRom
import com.manimarank.websitemonitor.utils.Utils.openAutoStartScreen
import com.manimarank.websitemonitor.utils.Utils.startWorkManager

class SettingsActivity : AppCompatActivity() {

    private lateinit var activitySettingsBinding: ActivitySettingsBinding
    private lateinit var btnMonitorInterval: LinearLayout
    private lateinit var btnMonitorStatusCodes: LinearLayout
    private lateinit var layoutEnableAutoStart: LinearLayout
    private lateinit var btnEnableAutoStart: TextView
    private lateinit var switchNotifyOnlyServerIssues: SwitchMaterial
    private lateinit var txtIntervalDetails: AppCompatTextView
    private lateinit var txtStatusCodesDetails: AppCompatTextView
    private lateinit var statusCodesList: Map<Int, String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activitySettingsBinding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(activitySettingsBinding.root)
        btnMonitorInterval = activitySettingsBinding.btnMonitorInterval
        btnMonitorStatusCodes = activitySettingsBinding.btnMonitorStatusCodes
        layoutEnableAutoStart = activitySettingsBinding.layoutEnableAutoStart
        btnEnableAutoStart = activitySettingsBinding.btnEnableAutoStart
        switchNotifyOnlyServerIssues = activitySettingsBinding.switchNotifyOnlyServerIssues
        txtIntervalDetails = activitySettingsBinding.txtIntervalDetails
        txtStatusCodesDetails = activitySettingsBinding.txtStatusCodesDetails

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btnMonitorInterval.setOnClickListener { showIntervalChooseDialog() }

        statusCodesList = Utils.statusCodesList.toSortedMap()
        btnMonitorStatusCodes.setOnClickListener { showStatusCodeChooseDialog() }

        layoutEnableAutoStart.visibility = if (isCustomRom()) View.VISIBLE else View.GONE
        btnEnableAutoStart.setOnClickListener { openAutoStartScreen(this) }

        updateIntervalTimeOnUi()

        updateStatusCodesOnUi()

        switchNotifyOnlyServerIssues.isChecked = SharedPrefsManager.customPrefs.getBoolean(NOTIFY_ONLY_SERVER_ISSUES, false)
        switchNotifyOnlyServerIssues.setOnCheckedChangeListener { _, isChecked ->
            SharedPrefsManager.customPrefs[NOTIFY_ONLY_SERVER_ISSUES] = isChecked
        }

        activitySettingsBinding.switchDarkMode.isChecked = SharedPrefsManager.customPrefs.getBoolean(IS_DARK_MODE_ENABLED, false)
        activitySettingsBinding.switchDarkMode.text  = getString(if (activitySettingsBinding.switchDarkMode.isChecked) R.string.disable_dark_mode else R.string.enable_dark_mode)
        activitySettingsBinding.switchDarkMode.setOnCheckedChangeListener { _, isChecked ->
            SharedPrefsManager.customPrefs[IS_DARK_MODE_ENABLED] = isChecked
            activitySettingsBinding.switchDarkMode.text  = getString(if (isChecked) R.string.disable_dark_mode else R.string.enable_dark_mode)
            Utils.enableDarkMode(isChecked)
        }
    }

    private fun showIntervalChooseDialog() {

        val alertBuilder = AlertDialog.Builder(this)
        alertBuilder.setTitle(getString(R.string.choose_interval))

        val checkedItem = valueList.indexOf(SharedPrefsManager.customPrefs.getInt(MONITORING_INTERVAL, 60))
        alertBuilder.setSingleChoiceItems(
            nameList,
            checkedItem
        ) { dialog: DialogInterface, which: Int ->
            SharedPrefsManager.customPrefs[MONITORING_INTERVAL] = valueList[which]
            startWorkManager(this, true)
            updateIntervalTimeOnUi()
            dialog.dismiss()
        }
        alertBuilder.setNegativeButton(getString(R.string.cancel), null)
        val dialog = alertBuilder.create()
        dialog.show()
    }

    private fun updateIntervalTimeOnUi() {
        txtIntervalDetails.text = getMonitorTime()
    }

    private fun showStatusCodeChooseDialog() {

        val alertBuilder = AlertDialog.Builder(this)
        alertBuilder.setTitle(getString(R.string.choose_status_codes))


        val selectedStatusCodes = Utils.monitorStatusCodes() ?: statusCodesList.keys.map { "$it" }.toSet()
        val checkedItems = statusCodesList.map { selectedStatusCodes.contains("${it.key}") }.toBooleanArray()

        alertBuilder.setMultiChoiceItems(
            statusCodesList.map { "${it.key}: ${it.value}" }.toTypedArray(),
            checkedItems
        ) { dialog: DialogInterface, which: Int, isChecked: Boolean ->
            checkedItems[which] = isChecked
        }
        alertBuilder.setPositiveButton(getString(R.string.ok)) { dialog, which ->
            val finalSelectedItems = ArrayList<Int>()
            statusCodesList.keys.forEachIndexed { index, i ->
                if (checkedItems[index]) {
                    finalSelectedItems.add(i)
                }
            }

            val finalSet: Set<String> = finalSelectedItems.sorted().map { "$it" }.toSet()
            SharedPrefsManager.customPrefs[MONITORING_STATUS_CODES] = finalSet
        }
        alertBuilder.setNegativeButton(getString(R.string.cancel), null)
        alertBuilder.setOnDismissListener {
            updateStatusCodesOnUi()
        }
        val dialog = alertBuilder.create()
        dialog.show()
    }

    private fun updateStatusCodesOnUi() {
        val list = Utils.monitorStatusCodes()?.sorted()
        txtStatusCodesDetails.text = when {
            list == null || statusCodesList.size == list.size -> getString(R.string.all_status_codes_monitored)
            list.isEmpty() -> getString(R.string.no_status_codes_monitored)
            else -> list.joinToString()
        }
    }
}